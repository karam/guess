## Guess

This is my first C program; a basic number-guessing game.

### Usage

Just compile the source file:

```sh
cc guess.c -o guess
```

Additionally, pass `-DTRIES=$NUMBER` to set the `$NUMBER` of tries for each
game (defaults to ten).

Run `./guess -h` for a help message.

### License

Licensed under the [MIT/Expat license](LICENSE).
> Copyright (C) 2020 Karam Assany (<karam.assany@disroot.org>)

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#ifndef TRIES
#define TRIES 10
#endif

/*******************************************************************************
 *
 * Guess - a basic number-guessing game
 *
 * Copyright (C) 2020 Karam Assany <karam.assany@disroot.org>
 * Licensed under the MIT/Expat license.
 * See the LICENSE file accompanied with this source file.
 *
 ******************************************************************************/

void swap_int(int *l, int *r)
{
    int t = *l; *l = *r; *r = t;
}

int main(int argc, char *argv[])
{
    int left, right, current, input;
    signed char tries = TRIES;

    if (argc == 2)
    {
        if (
            (strcmp(argv[1], "-h") == 0) ||
            (strcmp(argv[1], "--help") == 0)
           )
        {
            printf(
"Guess - a basic number-guessing game\n"
"\nSyntax:\n"
    "\tguess            (for playing the game)\n"
    "\tguess -h|--help  (for showing this help message)\n"
"\nHow to play:\n"
    "\tYou start the game by choosing two endpoints of a range of integers,\n"
    "\tthen you have %d tries to guess the right number in this range. You\n"
    "\thave to guess it right before no tries are left. When you do, you win.\n"
"\nCopyright (C) 2020 Karam Assany <karam.assany@disroot.org>\n"
"Licensed under the MIT/Expat license. See <https://codeberg.org/karam/guess>\n"
                , TRIES);
            return EXIT_SUCCESS;
        }
        else
        {
            printf("Argument \"%s\" not understood. See '--help'.\n", argv[1]);
            return EXIT_FAILURE;
        }
    }
    
    if (argc > 1)
    {
        puts("Arguments not understood. See '--help'.");
        return EXIT_FAILURE;
    }

    puts("Welcome to Guess!");

    game:

    printf("\nPlease enter one side of the range: ");
    scanf("%d", &left);
    printf("Please enter the other side of the range: ");
    scanf("%d", &right);

    if (left > right)
        swap_int(&left, &right);

    printf("Chosen difficulty is %d\n", right - left);

    srand(time(0));
    current = (rand() % (right - left + 1)) + left;

    while (tries > 0)
    {
        printf("You have %d tries left.\n", tries);

        printf("Guess a number in the range [%d,%d]: ", left, right);
        scanf("%d", &input);

        if ( (input < left) || (input > right) )
        {
            puts("You have input a number out of the range.\n");
            continue;
        }
        
        if ( input < current)
        {
            puts("It is larger!");
            left = input;
        }
        
        if ( input > current)
        {
            puts("It is smaller!");
            right = input;
        }

        if (input == current)
            break;

        tries--;
    }

    if (tries > 0)
        printf("Congratulation! It was %d! You won.\n", current);
    else
        printf("No tries left. You lost. It was %d btw.\n", current);

    printf("\nWanna play again [y for yes]? ");
    scanf(" %c", &input);
    if (input == 'y')
        goto game;

    return EXIT_SUCCESS;
}
